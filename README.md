# Adonis JS Boilerplate API

## Installation

```
npm install
```

## Setup

```
DB_CONNECTION=pg
PG_HOST=localhost
PG_PORT=5432
PG_USER=postgres
PG_PASSWORD=1234
```

## Migration & Seeder

```
node ace migration:fresh
node ace db:seed -i
```

Pilih database/seeders/MainSeeder/Index

## RUN

See package.json
