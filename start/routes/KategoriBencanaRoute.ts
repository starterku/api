import Route from '@ioc:Adonis/Core/Route'
const routePath = '/kategori-bencana'

Route.group(() => {
  Route.get('/option-filter', 'KategoriBencanaController.optionFilter').middleware('authenticated')
}).prefix(routePath)
