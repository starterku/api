import Route from '@ioc:Adonis/Core/Route'
const routePath = '/laporan'

Route.group(() => {
  Route.get('/', 'LaporansController.index').middleware('authenticated')
  Route.get('/count', 'LaporansController.countReport').middleware('authenticated')
  Route.put('/:id/category', 'LaporansController.changeCategory').middleware('authenticated')
  Route.put('/:id/state', 'LaporansController.changeState').middleware('authenticated')
  Route.get('/:id/detail', 'LaporansController.detail').middleware('authenticated')
  Route.get('/:id/user', 'LaporansController.userReport')
}).prefix(routePath)
