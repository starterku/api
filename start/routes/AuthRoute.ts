import Route from '@ioc:Adonis/Core/Route'
const routePath = '/auth'

Route.group(() => {
  Route.post('/', 'AuthController.doLogin').middleware('authRequest')
  Route.get('/me', 'AuthController.me').middleware('authenticated')
}).prefix(routePath)
