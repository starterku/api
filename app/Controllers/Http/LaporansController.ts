import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import Laporan from '../../Models/Laporan'
const dayjs = require('dayjs')
require('dayjs/locale/id')
dayjs.locale('id')
import { DateTime } from 'luxon'

export default class LaporansController {
  private statusLaporan = ['', 'Laporan Dibuat', 'Ditindaklanjuti', 'Laporan Selesai']

  public async index({ request, response }: HttpContextContract) {
    const page = request.input('page', 1)
    const limit = request.input('limit')
    const categoryId = request.input('category_id')
    const keyword = request.input('keyword')

    const laporan = await Laporan.query()
      .preload('laporan_media')
      .preload('kategori_bencana')
      .if(categoryId, (query) => {
        if (categoryId !== 'null' && categoryId !== null) {
          query.where('category_id', categoryId)
        }
      })
      .if(keyword, (query) => {
        query.where('description', 'ilike', `%${keyword}%`)
        query.orWhere('name', 'ilike', `%${keyword}%`)
        query.orWhere('phone', 'ilike', `%${keyword}%`)
      })
      .orderBy('created_at', 'asc')
      .paginate(page, limit)

    response.json(laporan)
  }
  public async detail({ request, response }: HttpContextContract) {
    const id: string = request.param('id') as string
    const laporan = await Laporan.query()
      .preload('laporan_media')
      .preload('kategori_bencana')
      .where('id', id)
      .limit(1)

    if (laporan.length === 1) {
      const formatedLaporan = laporan.map((laporan) => {
        laporan.createdAt = dayjs(laporan.createdAt).format('dddd, DD MMMM YYYY - HH:mm')
        return laporan
      })
      response.status(200).json({ success: true, laporan: formatedLaporan[0] })
    } else {
      response.status(404).json({ success: false })
    }
  }
  public async userReport({ request, response }: HttpContextContract) {
    if (request.param('id')) {
      if (typeof request.param('id') === 'string') {
        const id = request.param('id')
        const laporan = await Laporan.query().where('id', id).preload('laporan_media')
        Laporan.hidePhoneNumbers(laporan)
        const formatedLaporan = laporan.map((laporan) => {
          laporan.createdAt = dayjs(laporan.createdAt).format('dddd, DD MMMM YYYY - HH:mm')
          laporan.status = this.statusLaporan[parseInt(laporan.status)]
          return laporan
        })

        if (laporan.length) {
          response.status(200).json({ success: true, data: formatedLaporan[0] })
        } else {
          response.status(404).json({ success: false, message: 'Data tidak ditemukan !' })
        }
      } else {
        response.status(400).json({ success: false, message: 'Kode tidak sesuai format !' })
      }
    } else {
      response.status(400).json({ success: false, message: 'Kode tidak dimuat !' })
    }
  }
  public async countReport({ response }: HttpContextContract) {
    const data = await Database.rawQuery(`
      SELECT 
        COUNT(*) AS total_laporan, 
        SUM(CASE WHEN DATE(created_at  AT TIME ZONE 'Asia/Jakarta') = CURRENT_DATE THEN 1 ELSE 0 END) AS jumlah_laporan_hari_ini, 
        SUM(CASE WHEN status = '1' THEN 1 ELSE 0 END) AS jumlah_laporan_1, 
        SUM(CASE WHEN status = '2' THEN 1 ELSE 0 END) AS jumlah_laporan_2, 
        SUM(CASE WHEN status = '3' THEN 1 ELSE 0 END) AS jumlah_laporan_3
      FROM 
        laporan;
    `)
    response.status(200).json(data.rows[0])
  }
  public async changeCategory({ request, response }: HttpContextContract) {
    const laporanId = request.param('id')
    const categoryId = request.body().category_id
    const laporan = await Laporan.findOrFail(laporanId)
    laporan.category_id = categoryId
    await laporan.save()
    response.status(201).json({ success: true })
  }
  public async changeState({ request, response }: HttpContextContract) {
    const laporanId = request.param('id')
    const status = request.body().state
    const laporan = await Laporan.findOrFail(laporanId)

    const dt = DateTime.fromISO(new Date().toISOString())

    if (status === '2') {
      laporan.processedAt = dt
      laporan.status = '2'
    } else if (status === '3') {
      laporan.finishedAt = dt
      laporan.status = '3'
    } else {
      laporan.status = '1'
      laporan.processedAt = null
      laporan.finishedAt = null
    }
    await laporan.save()
    response.status(201).json({ success: true })
  }
}
