import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import KategoriBencana from '../../Models/KategoriBencana'
export default class KategoriBencanaController {
  public async optionFilter({ response }: HttpContextContract) {
    const data = await KategoriBencana.query().orderBy('category_name', 'asc')
    response.status(200).json(data)
  }
}
