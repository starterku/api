import Hash from '@ioc:Adonis/Core/Hash'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import User from '../../../app/Models/User'
import { UserType } from '../../../types/user.types'
import jwt from 'jsonwebtoken'
import Env from '@ioc:Adonis/Core/Env'

export default class AuthController {
  public async doLogin({ request, response }: HttpContextContract) {
    try {
      const username: string = request.body().username
      const password: string = request.body().password
      const getUser = await User.query()
        .where('username', username)
        .orWhere('email', username)
        .where('status', '1')
        .preload('roles', (q) => {
          q.where('status', '1')
        })
      const userJSON = getUser.map((x) =>
        x.serialize({
          relations: {
            roles: {
              fields: ['id', 'name'],
            },
          },
        })
      )
      if (userJSON.length === 1) {
        const aUser = userJSON[0]
        const dataUser: UserType = {
          id: aUser.id,
          name: aUser.name,
          username: aUser.username,
          email: aUser.email,
          handphone: aUser.handphone,
          gender: aUser.gender,
          roles: aUser.roles as [],
        }
        if (await Hash.verify(aUser.password, password)) {
          const accessToken: string = jwt.sign(
            {
              data: dataUser,
            },
            Env.get('JWT_SECRET'),
            { expiresIn: '1d' }
          )
          let token = ''
          const tokenSplit = accessToken.split('.')
          for (let index = 0; index < tokenSplit.length; index++) {
            let strPartition = ''
            if (index === 0) {
              strPartition = 'bjn'
            } else if (index === 1) {
              strPartition = 'siu'
            } else if (index === 2) {
              strPartition = 'nego'
            }
            const tkn = this.strPartition(tokenSplit[index], strPartition, 10)

            token += tkn
            if (index !== tokenSplit.length - 1) {
              token += '.'
            }
          }
          return { token: token }
        } else {
          response.status(400).json({ message: 'Password yang dimasukkan salah !' })
        }
      } else {
        response.status(400).json({ message: 'Akun tidak ditemukan !' })
      }
    } catch (error) {
      response.badRequest(error.messages)
    }
  }
  public async me({ request, response }: HttpContextContract) {
    response.status(200).json((request as any).user)
  }
  private strPartition(main_string: string, ins_string: string, pos: number) {
    if (typeof pos === 'undefined') {
      pos = 0
    }
    if (typeof ins_string === 'undefined') {
      ins_string = ''
    }
    return main_string.slice(0, pos) + ins_string + main_string.slice(pos)
  }
}
