import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
export default class AuthRequest {
  public async handle({ request, response }: HttpContextContract, next: () => Promise<void>) {
    const requestValidation = schema.create({
      username: schema.string([rules.minLength(6)]),
      password: schema.string([rules.minLength(4)]),
    })
    try {
      await request.validate({
        schema: requestValidation,
        messages: {
          required: 'Kolom {{field}} belum diisi !',
        },
      })
      await next()
    } catch (error) {
      response.badRequest(error.messages)
    }
  }
}
