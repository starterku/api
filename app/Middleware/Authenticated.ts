import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import jwt from 'jsonwebtoken'
import Env from '@ioc:Adonis/Core/Env'

export default class Authenticated {
  public async handle({ response, request }: HttpContextContract, next: () => Promise<void>) {
    try {
      const token = request.header('Authorization')
      if (token) {
        const tokenSplit = token.split('.')

        let tokenUser = ''
        for (let index = 0; index < tokenSplit.length; index++) {
          let strPartition = ''

          if (index === 0) {
            strPartition = tokenSplit[index].slice(0, 10) + tokenSplit[index].slice(13)
          } else if (index === 1) {
            strPartition = tokenSplit[index].slice(0, 10) + tokenSplit[index].slice(13)
          } else if (index === 2) {
            strPartition = tokenSplit[index].slice(0, 10) + tokenSplit[index].slice(14)
          }
          tokenUser += strPartition
          if (index !== tokenSplit.length - 1) {
            tokenUser += '.'
          }
        }

        if (!tokenUser) {
          response.status(401).json({ message: 'User tidak valid' })
        } else {
          const decodeUser = jwt.verify(tokenUser, Env.get('JWT_SECRET'))
          // @ts-ignore
          request.user = decodeUser
          await next()
        }
      } else {
        response.status(401).json({ message: 'User tidak valid' })
      }
      //Authorization: 'Bearer TOKEN'
    } catch (error) {
      response.status(401).json({ message: error.message })
    }
  }
}
