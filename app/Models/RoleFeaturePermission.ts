import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class RoleFeaturePermission extends BaseModel {
  public static table = 'role_feature_permission'

  @column({ isPrimary: true })
  public id: number

  @column()
  public role_id: string

  @column()
  public feature_permission_kode: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
