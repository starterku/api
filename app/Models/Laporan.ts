import { DateTime } from 'luxon'
import {
  BaseModel,
  BelongsTo,
  belongsTo,
  column,
  hasMany,
  HasMany,
  HasOne,
  hasOne,
} from '@ioc:Adonis/Lucid/Orm'
import LaporanMedia from '../Models/LaporanMedia'
import KategoriBencana from '../Models/KategoriBencana'

export default class Laporan extends BaseModel {
  public static table = 'laporan'

  @column({ isPrimary: true })
  public id: string

  @column()
  public description: string

  @column()
  public phone: string

  @column()
  public name: string

  @column()
  public category_id: number

  @column()
  public step: string

  @column()
  public is_laporan_complete: Boolean

  @column()
  public is_step_1_complete: Boolean

  @column()
  public is_step_2_complete: Boolean

  @column()
  public is_step_3_complete: Boolean

  @column()
  public lat: number

  @column()
  public lng: number

  @column()
  public status: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column.dateTime()
  public processedAt: DateTime

  @column.dateTime()
  public finishedAt: DateTime

  public static async hidePhoneNumbers(laporans: Laporan[]) {
    laporans.forEach((laporan) => {
      if (laporan.phone) {
        const lengthToHide = 5 // jumlah digit terakhir yang ingin disembunyikan
        const visibleLength = laporan.phone.length - lengthToHide
        const hiddenDigits = '*'.repeat(lengthToHide)
        laporan.phone = laporan.phone.slice(0, visibleLength) + hiddenDigits
      }
    })
  }

  @hasMany(() => LaporanMedia)
  public laporan_media: HasMany<typeof LaporanMedia>

  @hasOne(() => KategoriBencana, {
    localKey: 'category_id',
    foreignKey: 'id',
  })
  public kategori_bencana: HasOne<typeof KategoriBencana>
}
