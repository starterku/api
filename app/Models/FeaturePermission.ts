import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class FeaturePermission extends BaseModel {
  public static table = 'feature_permission'

  @column({ isPrimary: true })
  public kode: string

  @column()
  public name: string

  @column()
  public feature_id: number

  @column()
  public status: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
