import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class LaporanMedia extends BaseModel {
  public static table = 'laporan_media'

  @column({ isPrimary: true })
  public id: string

  @column()
  public laporanId: string

  @column()
  public url: string

  @column()
  public caption: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
