import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class RoleFeature extends BaseModel {
  public static table = 'role_feature'

  @column({ isPrimary: true })
  public id: number

  @column()
  public role_id: string

  @column()
  public feature_id: number

  @column()
  public menu_id: number

  @column()
  public level: number

  @column()
  public parent: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
