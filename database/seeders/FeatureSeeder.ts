import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Feature from '../../app/Models/Feature'
export default class extends BaseSeeder {
  public async run() {
    await Feature.createMany([
      {
        name: 'Dashboard',
        url: '/system/dashboard',
        icon: 'bi bi-speedometer',
        is_internal_url: true,
      },
      {
        name: 'Master Data',
        url: '#',
        icon: 'bi bi-database-fill-add',
        is_internal_url: true,
      },
      {
        name: 'Kategori',
        url: '/system/category',
        icon: 'bi bi-bookmark-star-fill',
        is_internal_url: true,
      },
      {
        name: 'Konfigurasi',
        url: '#',
        icon: 'bi bi-sliders2',
        is_internal_url: true,
      },
      {
        name: 'Manajemen User',
        url: '/system/user-management',
        icon: 'bi bi-people-fill',
        is_internal_url: true,
      },
      {
        name: 'Fitur & Akses',
        url: '/system/feature-and-permission',
        icon: 'bi bi-people-fill',
        is_internal_url: true,
      },
    ])
  }
}
