import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Application from '@ioc:Adonis/Core/Application'

export default class IndexSeeder extends BaseSeeder {
  private async runSeeder(Seeder: { default: typeof BaseSeeder }) {
    /**
     * Do not run when not in dev mode and seeder is development
     * only
     */
    if (Seeder.default.developmentOnly && !Application.inDev) {
      return
    }

    await new Seeder.default(this.client).run()
  }

  public async run() {
    await this.runSeeder(await import('../KategoriBencanaSeeder'))
    await this.runSeeder(await import('../RoleSeeder'))
    await this.runSeeder(await import('../UserSeeder'))
    await this.runSeeder(await import('../UserRoleSeeder'))
    await this.runSeeder(await import('../MenuSeeder'))
    await this.runSeeder(await import('../FeatureSeeder'))
    await this.runSeeder(await import('../RoleFeatureSeeder'))
    await this.runSeeder(await import('../FeaturePermissionSeeder'))
    await this.runSeeder(await import('../RoleFeaturePermissionSeeder'))
  }
}
