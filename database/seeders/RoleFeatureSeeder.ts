import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import RoleFeature from '../../app/Models/RoleFeature'
export default class extends BaseSeeder {
  public async run() {
    await RoleFeature.createMany([
      {
        menu_id: 1,
        role_id: 'SUPER',
        level: 1,
        feature_id: 1,
      },
      {
        menu_id: 1,
        role_id: 'SUPER',
        level: 1,
        feature_id: 2,
      },
      {
        menu_id: 1,
        role_id: 'SUPER',
        level: 1,
        feature_id: 4,
      },
      {
        menu_id: 1,
        role_id: 'SUPER',
        level: 2,
        feature_id: 3,
        parent: 2,
      },
      {
        menu_id: 1,
        role_id: 'SUPER',
        level: 2,
        feature_id: 5,
        parent: 4,
      },
      {
        menu_id: 1,
        role_id: 'SUPER',
        level: 2,
        feature_id: 6,
        parent: 4,
      },
    ])
  }
}
