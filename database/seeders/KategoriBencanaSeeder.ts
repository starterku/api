import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import KategoriBencana from '../../app/Models/KategoriBencana'

export default class extends BaseSeeder {
  public async run() {
    await KategoriBencana.createMany([
      {
        category_name: '-',
      },
      {
        category_name: 'Banjir Bandang',
      },
      {
        category_name: 'Banjir Bengawan Solo',
      },
      {
        category_name: 'Banjir Luapan',
      },
      {
        category_name: 'Cuaca Ekstrim (Angin Kencang / Puting Beliung)',
      },
      {
        category_name: 'Gempa Bumi',
      },
      {
        category_name: 'Kebakaran Hutan dan Lahan',
      },
      {
        category_name: 'Kebakaran Rumah',
      },
      {
        category_name: 'Kegagalan Industri',
      },
      {
        category_name: 'Kejadian Lain-lain',
      },
      {
        category_name: 'Kekeringan',
      },
      {
        category_name: 'Tanah Longsor',
      },
    ])
  }
}
