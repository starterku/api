import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Menu from '../../app/Models/Menu'
export default class extends BaseSeeder {
  public async run() {
    await Menu.createMany([
      {
        name: 'Sidebar',
      },
      {
        name: 'Navbar Landing Page',
      },
    ])
  }
}
