import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import User from '../../app/Models/User'
import { v4 as uuidv4 } from 'uuid'

export default class extends BaseSeeder {
  public async run() {
    await User.createMany([
      {
        id: uuidv4(),
        username: 'mr-super',
        name: 'Mr X',
        password: '@Superuser99',
        email: 'superuser@gmail.com',
      },
      {
        id: uuidv4(),
        username: 'mr-administrator',
        name: 'Mr Administrator',
        password: '@admin123#',
        email: 'administrator@gmail.com',
      },
      {
        id: uuidv4(),
        username: 'mr-user',
        name: 'Mr User',
        password: '123456',
        email: 'user@gmail.com',
      },
    ])
  }
}
