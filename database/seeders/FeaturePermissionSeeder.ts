import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import FeaturePermission from '../../app/Models/FeaturePermission'

export default class extends BaseSeeder {
  public async run() {
    await FeaturePermission.createMany([
      {
        kode: 'ACCESS_PAGE_DASHBOARD',
        name: 'View Page',
        feature_id: 1,
      },
      {
        kode: 'ACCESS_PAGE_CATEGORY',
        name: 'View Page',
        feature_id: 3,
      },
      {
        kode: 'GET_DATA_CATEGORY',
        name: 'Get',
        feature_id: 3,
      },
      {
        kode: 'ADD_DATA_CATEGORY',
        name: 'Add',
        feature_id: 3,
      },
      {
        kode: 'UPDATE_DATA_CATEGORY',
        name: 'Update',
        feature_id: 3,
      },
      {
        kode: 'DELETE_DATA_CATEGORY',
        name: 'Delete',
        feature_id: 3,
      },
      {
        kode: 'ACCESS_PAGE_USER',
        name: 'View Page',
        feature_id: 5,
      },
      {
        kode: 'GET_DATA_USER',
        name: 'Get',
        feature_id: 5,
      },
      {
        kode: 'ADD_DATA_USER',
        name: 'Add',
        feature_id: 5,
      },
      {
        kode: 'UPDATE_DATA_USER',
        name: 'Update',
        feature_id: 5,
      },
      {
        kode: 'DELETE_DATA_USER',
        name: 'Delete',
        feature_id: 5,
      },
      {
        kode: 'BLOCK_USER',
        name: 'Block User',
        feature_id: 5,
      },
      {
        kode: 'RESET_PASSWORD_USER',
        name: 'Reset Password',
        feature_id: 5,
      },
    ])
    // Write your database queries inside the run method
  }
}
