import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import RoleFeaturePermission from '../../app/Models/RoleFeaturePermission'
export default class extends BaseSeeder {
  public async run() {
    const permissionStarter = [
      'ACCESS_PAGE_DASHBOARD',
      'ACCESS_PAGE_CATEGORY',
      'GET_DATA_CATEGORY',
      'ADD_DATA_CATEGORY',
      'UPDATE_DATA_CATEGORY',
      'DELETE_DATA_CATEGORY',
      'ACCESS_PAGE_USER',
      'GET_DATA_USER',
      'ADD_DATA_USER',
      'UPDATE_DATA_USER',
      'DELETE_DATA_USER',
      'BLOCK_USER',
      'RESET_PASSWORD_USER',
    ]
    let featurePermission: Object[] = []

    for (let index = 0; index < permissionStarter.length; index++) {
      const ps = permissionStarter[index]
      featurePermission.push({ role_id: 'SUPER', feature_permission_kode: ps })
    }
    await RoleFeaturePermission.createMany(featurePermission)
  }
}
