import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import User from '../../app/Models/User'
import UserRole from '../../app/Models/UserRole'
import { v4 as uuidv4 } from 'uuid'

export default class extends BaseSeeder {
  public async run() {
    const userone = await User.findBy('username', 'mr-super')
    await UserRole.create({
      id: uuidv4(),
      user_id: userone?.$attributes.id,
      role_id: 'SUPER',
    })

    const usertwo = await User.findBy('username', 'mr-administrator')
    await UserRole.create({
      id: uuidv4(),
      user_id: usertwo?.$attributes.id,
      role_id: 'ADMIN',
    })

    const userthree = await User.findBy('username', 'mr-user')
    await UserRole.create({
      id: uuidv4(),
      user_id: userthree?.$attributes.id,
      role_id: 'USER',
    })
  }
}
