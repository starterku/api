import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Role from '../../app/Models/Role'

export default class extends BaseSeeder {
  public async run() {
    await Role.createMany([
      {
        id: 'SUPER',
        name: 'Superadmin',
      },
      {
        id: 'ADMIN',
        name: 'Adminstrator',
      },
      {
        id: 'USER',
        name: 'User',
      },
      {
        id: 'GUEST',
        name: 'Guest',
      },
    ])
  }
}
