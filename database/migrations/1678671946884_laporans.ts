import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'laporan'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.string('id', 25).primary()
      table.string('description', 255)
      table.string('phone', 16)
      table.string('name')
      table.string('step', 1)
      table.integer('category_id', 1).references('id').inTable('kategori_bencana').defaultTo(1)
      table.boolean('is_step_1_complete')
      table.boolean('is_step_2_complete')
      table.boolean('is_step_3_complete')
      table.double('lat')
      table.double('lng')
      table.boolean('is_laporan_complete')
      table
        .enum('status', ['1', '2', '3'])
        .comment('1=laporan dibuat, 2=laporan ditindaklanjuti, 3=laporan selesai')
        .defaultTo('1')
      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('processed_at', { useTz: true })
      table.timestamp('finished_at', { useTz: true })
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
