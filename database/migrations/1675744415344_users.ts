import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'users'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.string('id').primary()
      table.string('username', 18).notNullable()
      table.string('name', 255).notNullable()
      table.string('email', 255).unique()
      table.string('handphone', 15).unique()
      table.enum('gender', ['L', 'P'])
      table.string('password', 255).notNullable()
      table.string('status').comment('1=aktif, 2=waiting confirmation, 3=block').defaultTo('1')
      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
