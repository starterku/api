export type UserType = {
  id: string
  name: string
  username: string
  email?: string
  handphone?: string
  gender: string
  roles: []
}
